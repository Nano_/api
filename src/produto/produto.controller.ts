import { Controller, Get, Param, Body, Post, Put, Delete } from '@nestjs/common';
import { ProdutoService } from './shared/produto.service';
import { Produto } from './shared/produto';



@Controller('produto')
export class ProdutoController {

    constructor( private  produtoService: ProdutoService){ }

    


    @Get()
    async getAll() : Promise<Produto[]>{
        return this.produtoService.getAll();

    }
    
    // @Get(':clientes')
    // async getAllCliente(@Param('clientes') clientes: string) : Promise<Produto[]>{  
    //     clientes = "cliente";    
    //     return this.produtoService.getAllCliente(clientes);
    // }

    @Get(':produtos')
    async getAllProdutos(@Param('produtos') produtos: string) : Promise<Produto[]>{  
        produtos = "produto";    
        return this.produtoService.getAllCliente(produtos);
    }


    @Get(':id')
    async getById(@Param('id') id: string) : Promise<Produto>{
        return this.produtoService.getById(id);

    }

    @Post(':create')
    async create(@Body() produto: Produto)  : Promise<Produto>{
        return this.produtoService.create(produto);
    }


    @Put(':id')
    async update(@Param('id') id: string, @Body() produto: Produto): Promise<Produto>{
        return this.produtoService.update(id, produto);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        this.produtoService.delete(id);
    }






}
