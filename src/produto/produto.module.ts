import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';
import { ProdutoController } from './produto.controller';
import { ProdutoService } from './shared/produto.service';
import { ProdutoSchema } from './schemas/produto.schema';


@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'Produto', schema: ProdutoSchema}])
    ],
    controllers: [ProdutoController],
    providers: [ProdutoService]
})
export class ProdutoModule {}
