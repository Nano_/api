import { Document } from 'mongoose';


export class Produto extends Document{
    nomeP: String; 
    marca: String;
    preco: ConstrainDouble;
    quantidade: Int32List;
    tag: String;
}
