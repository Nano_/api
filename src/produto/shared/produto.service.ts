import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Produto } from './produto';
import { Model } from 'mongoose';

@Injectable()
export class ProdutoService {

    constructor(@InjectModel('Produto') private readonly produtoModel: Model<Produto>) {}
    

    async getAll(){
        return await this.produtoModel.find().exec();
    }
    async getAllCliente(tag: string){
        return await this.produtoModel.find({tag: tag}).exec();
    }

    async getById(id: string){
        return await this.produtoModel.findById(id).exec();
        
    }

    async create(produto: Produto){
        const createProduto = new this.produtoModel(produto);
        return await createProduto.save();
    }

    async update(id: string, produto: Produto){ 
        
        await this.produtoModel.updateOne({ _id: id }, produto).exec();
        return this.getById(id);
        
    }

    async delete(id: string){
        return await this.produtoModel.deleteOne({ _id: id }).exec();
    }



}
